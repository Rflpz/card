//
//  RLCardViewController.swift
//  CardTest
//
//  Created by Rafael Lopez on 17/12/14.
//  Copyright (c) 2014 Rflpz. All rights reserved.
//

import UIKit

class RLCardViewController: UIViewController,UITableViewDelegate {

    @IBOutlet var tableView: UITableView!
    var items: [String] = ["first card", "second card", "third card"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "PAYMENT"
    }
    func tableView(tableView: UITableView!, numberOfRowsInSection section:    Int) -> Int {
        return items.count
    }
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        var cell_ : UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("CELL_ID") as? UITableViewCell
        if(cell_ == nil)
        {
            cell_ = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "CELL_ID")
        }
        
        cell_!.textLabel!.text = items[indexPath.row]
        
        return cell_!
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let addCardViewController = RLAddCardViewController(nibName: "RLAddCardViewController", bundle: nil)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.navigationController?.pushViewController(addCardViewController, animated: true)
    }
    
}
