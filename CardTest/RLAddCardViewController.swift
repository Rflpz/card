//
//  RLAddCardViewController.swift
//  CardTest
//
//  Created by Rafael Lopez on 17/12/14.
//  Copyright (c) 2014 Rflpz. All rights reserved.
//

import UIKit

//FIXME: Cambiar esto por una enumeración
let visa = "Visa"
let masterCard = "MasterCard"
let amex = "American Express"
let unknown = "Unknown"

class RLAddCardViewController: UIViewController,UIPickerViewDelegate,UITextFieldDelegate {
//MARK: Declare the global objects
    //FIXME: ¿Usas outlets para no conectarlos?
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtAlias: UITextField!
    @IBOutlet weak var txtCardHolder: UITextField!
    @IBOutlet weak var txtCVV: UITextField!
    @IBOutlet weak var imageBrandView: UIImageView!
    
    @IBOutlet weak var datePicker: UIPickerView!
    
    @IBOutlet weak var cardHolderLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var aliasLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var cvvLabel: UILabel!
    
    var cardInfo = NSMutableDictionary()
    var buttonRight: UIBarButtonItem!
    
    
    let pickerData = [
        ["01","02","03","04","05","06","07","08","09","10","11","12"],
        ["14","15","16","17","18","19","20","21","22","23","24","25"]
    ]
    
    enum PickerComponent:Int {
        case month = 0
        case year = 1
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeCardInfo()
        datePicker.delegate = self
        datePicker.selectRow(0, inComponent: PickerComponent.month.rawValue, animated: true)
        updateDate()
        addSaveDataButton()
       // println(cardInfo)
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        initializeAlphaElements()
    }
    
    //MARK: Hide all the objects that must be hidden
    func initializeAlphaElements() {
        txtCardHolder.alpha = 0.0
        txtCardNumber.alpha = 0.0
        txtCVV.alpha = 0.0
        cardHolderLabel.alpha = 0.0
        cardNumberLabel.alpha = 0.0
        cvvLabel.alpha = 0.0
        yearLabel.alpha = 0.0
        monthLabel.alpha = 0.0
        imageBrandView.alpha = 0.0
        datePicker.alpha = 0.0
        buttonRight.enabled = false
    }
    
    func initializeCardInfo() {
        cardInfo.setObject("", forKey: "cardNumber")
        cardInfo.setObject(false, forKey: "luhn")
        cardInfo.setObject("", forKey: "brand")
        cardInfo.setObject("", forKey: "CVV")
        cardInfo.setObject("", forKey: "image")
        cardInfo.setObject("", forKey: "alias")
        cardInfo.setObject("", forKey: "yearExp")
        cardInfo.setObject("", forKey: "monthExp")
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
//MARK: Verify the UITextFields and animations
    @IBAction func aliasDone(sender: AnyObject) {
        if (!txtAlias.text.isEmpty) {
            showUITextField(txtCardHolder, show: true)
            showUILabel(cardHolderLabel, show: true)
        }
        else {
            showUILabel(cardHolderLabel, show: false)
            showUILabel(cardNumberLabel, show: false)
            showUILabel(cvvLabel, show: false)
            showUILabel(monthLabel, show: false)
            showUILabel(yearLabel, show: false)
            showUITextField(txtCardHolder, show: false)
            showUITextField(txtCardNumber, show: false)
            showUITextField(txtCVV, show: false)
            datePicker.alpha = 0
            imageBrandView.alpha = 0
            setImageToImgView("")
            buttonRight.enabled = false
        }
    }
    
    @IBAction func cardHolderDone(sender: AnyObject) {
        if (!txtCardHolder.text.isEmpty) {
            showUITextField(txtCardNumber, show: true)
            showUILabel(cardNumberLabel, show: true)
            imageBrandView.alpha = 1.0
        }
        else {
            showUILabel(cardNumberLabel, show: false)
            showUILabel(cvvLabel, show: false)
            showUILabel(monthLabel, show: false)
            showUILabel(yearLabel, show: false)
            
            showUITextField(txtCardNumber, show: false)
            showUITextField(txtCVV, show: false)
            
            datePicker.alpha = 0
            imageBrandView.alpha = 0
            setImageToImgView("")
            buttonRight.enabled = false
        }
    }
    
    @IBAction func cvvDone(sender: AnyObject) {
        if (isValidCVV(txtCVV.text, brand: cardInfo["brand"] as String)) {
            datePicker.alpha = 1.0
            buttonRight.enabled = true
            showUILabel(monthLabel, show: true)
            showUILabel(yearLabel, show: true)
        }
        else {
            showUILabel(monthLabel, show: false)
            showUILabel(yearLabel, show: false)
            buttonRight.enabled = false
            datePicker.alpha = 0
            //[datePicker  .selectRow(3, inComponent: 1, animated: true)]
        }
    }
    
    @IBAction func valueChange(sender: AnyObject) {
        if (isNumericOnlyString(txtCardNumber.text)) {
            validateCardBrandImage()
            validateLuhnToUITextFiled()
            //println(cardInfo)
        }
    }
    
    func addSaveDataButton() {
        UIBarButtonItemStyle.Plain
        buttonRight = UIBarButtonItem(title: "Save", style: .Plain, target: self, action: ("saveCard"))
        self.navigationItem.rightBarButtonItem = buttonRight
    }
    
    func saveCard() {
        cardInfo.setValue(txtCardNumber.text, forKey: "cardNumber")
        cardInfo.setValue(txtCVV.text, forKey: "CVV")
        cardInfo.setValue(pickerData[PickerComponent.month.rawValue][datePicker.selectedRowInComponent(PickerComponent.month.rawValue)], forKey: "monthExp")
        cardInfo.setValue(pickerData[PickerComponent.year.rawValue][datePicker.selectedRowInComponent(PickerComponent.year.rawValue)], forKey: "yearExp")
        
        if (isValidDateCard(cardInfo["monthExp"] as String, year: cardInfo["yearExp"] as String)) {
            if (isValidLength(cardInfo["cardNumber"] as String, brand: cardInfo["brand"] as String)) {
                if (isLuhnValidString(cardInfo["cardNumber"] as String)) {
                    if (isValidCVV(cardInfo["CVV"] as String, brand: cardInfo["brand"] as String)) {
                        if (isNumericOnlyString(cardInfo["cardNumber"] as String)) {
                            cardInfo.setValue(txtAlias.text, forKey: "alias")
                            println(cardInfo)
                            println("Tarjeta guardada")
                        }
                    }
                }
            }
        }
    }
    
    func validateCardBrandImage() {
        if (countElements(txtCardNumber.text) >= 2) {
            if (isSuportedBrand(brandName(txtCardNumber.text))) {
                cardInfo.setObject(brandName(txtCardNumber.text) as String, forKey: "brand")
                cardInfo.setObject(setImageOfBrand(cardInfo["brand"] as String), forKey: "image")
                setImageToImgView(cardInfo["image"] as String)
                txtCVV.text = ""
            }
            else {
                cardInfo.setObject(unknown, forKey:"brand")
                setImageToImgView(unknown)
                txtCVV.text = ""
            }
        }
        else {
            setImageToImgView("")
        }
    }
    
    func setImageToImgView(image: String) {
        var imageBrand = UIImage(named: image)
        self.imageBrandView.image = imageBrand
    }
    
    func textField(textField: UITextField!, shouldChangeCharactersInRange range: NSRange, replacementString string: String!) -> Bool {
        if (textField .isEqual(txtCardNumber)) {
            if (cardInfo["brand"] as String == amex) {
                var newLength = countElements(textField.text!) + countElements(string!) - range.length
                return newLength <= 15
            }
            else {
                var newLength = countElements(textField.text!) + countElements(string!) - range.length
                return newLength <= 16
            }
        }
        
        if (textField .isEqual(txtCVV)) {
            if (cardInfo["brand"] as String == visa  || cardInfo["brand"] as String == masterCard ) {
                var newLength = countElements(textField.text!) + countElements(string!) - range.length
                return newLength <= 3
            }
            else {
                var newLength = countElements(textField.text!) + countElements(string!) - range.length
                return newLength <= 4
            }
  
        }
        return true
    }
    
    func validateLuhnToUITextFiled() {
        if (isValidLength(txtCardNumber.text, brand: cardInfo["brand"] as String)) {
            if (isLuhnValidString(txtCardNumber.text)) {
                cardInfo.setValue(true, forKey: "luhn")
                //Si el número de tarjeta es válido se aparece lo correspondiente al cvv
                showUITextField(txtCVV, show: true)
                showUILabel(cvvLabel, show: true)
            }
            else {
                cardInfo.setValue(false, forKey: "luhn")
                setImageToImgView(unknown)
            }
        }
        else {
            showUILabel(cvvLabel, show: false)
            showUILabel(monthLabel, show: false)
            showUILabel(yearLabel, show: false)
            
            showUITextField(txtCVV, show: false)
            buttonRight.enabled = false
            datePicker.alpha = 0
        }
    }
}

extension RLAddCardViewController {
    func brandName(number: String) ->String {
        if (number.isEmpty || number.utf16Count < 2) {
            return unknown
        }
        
        var prefix = number.substringToIndex(advance(number.startIndex, 2)).toInt()
        
        if (40 <= prefix && prefix < 50) {
            return visa;
        }
        
        if (50 <= prefix && prefix <= 55) {
            return masterCard;
        }
        
        if (prefix == 34 || prefix == 37) {
            return amex;
        }
        
        return unknown
    }
    
    func isNumericOnlyString(number: String) -> Bool {
        var formatter = NSNumberFormatter()
        
        if var number = formatter.numberFromString(number) {
            return true
        }
        
        return false
    }
    
    func isSuportedBrand(brandCard: String) -> Bool{
        var supportedBrands = [visa,masterCard,amex]
        
        if (contains(supportedBrands, brandCard)) {
            return true
        }
        
        return false
    }
    
    func isLuhnValidString(number : String) -> Bool {
        let asciiOffset: UInt8 = 48
        let digits = Array(number.utf8).reverse().map{$0 - asciiOffset}
        
        let convert: (UInt8) -> (UInt8) = {
            let n = $0 * 2
            return n > 9 ? n - 9 : n
        }
        
        var sum: UInt8 = 0
        
        for (index, digit) in enumerate(digits) {
            if index & 1 == 1 {
                sum += convert(digit)
            } else {
                sum += digit
            }
        }
        
        return sum % 10 == 0
    }
    
    func setImageOfBrand(brand: String) -> String {
        var imageName: String!
        switch brand{
        case visa:
            imageName = "visa_logo"
            break
        case amex:
            imageName = "american_express_logo"
            break
        case masterCard:
            imageName = "mastercard_logo"
            break
        default:
            imageName = "unknown"
            break
        }
        return imageName
    }
    
    func isValidDateCard(month: String, year: String) -> Bool {
        var cardDate = year + "-" + month
        var status: Bool!
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM"
        var actualDate = dateFormatter.stringFromDate(NSDate())
        var dateComparisionResult:NSComparisonResult = actualDate.compare(cardDate)
        
        //FIXME: ¿Por qué no hacer un switch a través de los valores de la enumeración NSComparisonResult?
        if dateComparisionResult == NSComparisonResult.OrderedAscending {
            status = true
        }
        else if dateComparisionResult == NSComparisonResult.OrderedDescending {
            status = true
        }
        else if dateComparisionResult == NSComparisonResult.OrderedSame {
            status = false
        }
        
        return status
    }
    
    func isValidLength(number: String, brand: String) -> Bool {
        var lengthCard = number.utf16Count
        var status = false
        
        switch brand {
        case visa:
            if (lengthCard == 13 || lengthCard == 16) {
                status = true
            }
            break
        case amex:
            if (lengthCard == 15) {
                status = true
            }
            break
        case masterCard:
            if (lengthCard == 16) {
                status = true
            }
            break
        default:
            status = false
            break
        }
        
        return status
    }
    
    func isValidCVV(CVV: String, brand: String) -> Bool {
        var status = false

        switch brand {
        case visa:
            if (countElements(CVV) == 3) {
                status = true
            }
            break
        case amex:
            if (countElements(CVV) == 4) {
                status = true
            }
            break
        case masterCard:
            if (countElements(CVV) == 3) {
                status = true
            }
            break
        default:
            status = false
            break
        }
        
        return status
    }
}

extension RLAddCardViewController {
    func updateDate() {
        var monthComponent = PickerComponent.month.rawValue
        var yearComponent = PickerComponent.year.rawValue
        var month = pickerData[monthComponent][datePicker.selectedRowInComponent(monthComponent)]
        var year = pickerData[yearComponent][datePicker.selectedRowInComponent(yearComponent)]
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yy"
        var yearSystem = dateFormatter.stringFromDate(NSDate())
        dateFormatter.dateFormat = "MM"
        var monthSystem = dateFormatter.stringFromDate(NSDate())

        if (year.toInt()! == yearSystem.toInt()!) {
            if (month.toInt()! == monthSystem.toInt()!) {
                if (month.toInt()! == 12) {
                    [datePicker  .selectRow(0, inComponent: 0, animated: true)]
                    [datePicker  .selectRow(find(pickerData[1], String(yearSystem.toInt()! + 1))!, inComponent: 1, animated: true)]
                    updateDate()
                }
                else if (month.toInt()! == monthSystem.toInt()!) {
                    [datePicker  .selectRow(find(pickerData[0], String(monthSystem.toInt()! + 1))!, inComponent: 0, animated: true)]
                    updateDate()
                }
            }
            
            if (month.toInt()! < monthSystem.toInt()!) {
                [datePicker .selectRow(find(pickerData[0], monthSystem)!, inComponent: 0, animated: true)]
                updateDate()
            }
        }
        else if (year.toInt() < yearSystem.toInt()) {
            [datePicker .selectRow(find(pickerData[1], yearSystem)!, inComponent: 1, animated: true)]
            [datePicker .selectRow(find(pickerData[0], monthSystem)!, inComponent: 0, animated: true)]
            updateDate()
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        updateDate()
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return pickerData.count
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData[component].count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return pickerData[component][row]
    }
}

extension RLAddCardViewController {
    func showUITextField(obj: UITextField, show: Bool) {
        UIView.animateWithDuration(0.3, animations: {
            if (show) {
                obj.alpha = 1.0
            }
            else {
                obj.text = ""
                obj.alpha = 0.0
            }
        })
    }
    
    func showUILabel(obj: UILabel, show: Bool) {
        UIView.animateWithDuration(0.5, animations: {
            if (show) {
                obj.alpha = 1
            }
            else {
                obj.alpha = 0
            }
        })
    }
}